package com.likenuuk.librarylikenuuk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.likenuuk.librarylikenuuk.Model.Messsage;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textview.MaterialTextView;
import com.likenuuk.librarylikenuuk.Constants.Constantes;
import com.likenuuk.librarylikenuuk.Mvp.Chat_MVP;
import com.likenuuk.librarylikenuuk.Mvp.Chat_Presenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatLikenuuk#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatLikenuuk extends Fragment implements Chat_MVP.View, Chat_MVP.OnItemClickListenerMessage {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static LikenuukChat_Form likenuukChatInit;


    //TODO Check broadCast

    // TODO: Components


    private View view;

    // TODO: layout chat
    private final String TAG =getTag();
    private ConstraintLayout layout_chat;
    private AdapterChat adapterChat;
    private ImageButton btnSendMessage;
    private ImageButton btnGallery;
    private AppCompatEditText message_AppCompatEditText;
    private List<Messsage> messsage= new ArrayList<>();
    private RecyclerView recyclerView;
    private MaterialTextView message_today;
    private ConstraintLayout conatiner_chat;

    private static final String DESCRIBABLE_KEY = "describable_key";

    // TODO: layout error
    private ConstraintLayout layout_error;
    private MaterialTextView msg_err;
    private ImageView img_err;



    private Chat_Presenter presenter;

    public ChatLikenuuk() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param likenuukChat_form Change data.
     * @return A new instance of fragment ChatLikenuuk.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatLikenuuk newInstance(LikenuukChat_Form likenuukChat_form) {
        ChatLikenuuk fragment = new ChatLikenuuk();
        Bundle args = new Bundle();
        args.putParcelable("", likenuukChat_form);

        likenuukChatInit = likenuukChat_form;
        //  args.putSerializable(DESCRIBABLE_KEY,likenuukChat_init);

        //args.putString(URL_BASE, url_base);
        //args.putString(ENDPOINT, endpoint);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /* Intent i = getActivity().getIntent();
        likenuukChatInit = (LikenuukChat_Init) i.getSerializableExtra("LikenuukChat_Init_object");



        if (getArguments() != null) {

            url_base = getArguments().getString(URL_BASE);
            endpoint = getArguments().getString(ENDPOINT);

            likenuukChatInit = (LikenuukChat_Init) getArguments().getSerializable(
                    DESCRIBABLE_KEY);
        }*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // TODO: Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_chat_likenuuk, container, false);
        setContentView(view);

        presenter = new Chat_Presenter(this);
        presenter.setView(this);

        presenter.getChatService();

        adapterChat = new AdapterChat(messsage,getContext(),this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapterChat);

        return view;


    }



    void setContentView(View view){


        layout_chat = view.findViewById(R.id.layout_chat);
        layout_error = view.findViewById(R.id.layout_error);
        btnGallery = view.findViewById(R.id.btnGallery);
        btnSendMessage = view.findViewById(R.id.btnSendMessage);
        recyclerView = view.findViewById(R.id.recyclerView);
        message_today = view.findViewById(R.id.message_today);
        conatiner_chat = view.findViewById(R.id.container_chat);

         message_AppCompatEditText = view.findViewById(R.id.etMessage);

           btnGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    messsage.add(new Messsage(0,"Como puedo ayudarlo?"));
                    adapterChat.notifyItemInserted(messsage.size()-1);
                    recyclerView.getLayoutManager().scrollToPosition(messsage.size()-1);

                    //   Toast.makeText(getContext(),"click",Toast.LENGTH_LONG).show();
                }
            });

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!message_AppCompatEditText.getText().toString().isEmpty()) {
                    messsage.add(new Messsage(1,message_AppCompatEditText.getText().toString()));
                    adapterChat.notifyItemInserted(messsage.size()-1);
                    recyclerView.getLayoutManager().scrollToPosition(messsage.size()-1);

                    message_AppCompatEditText.setText("");

                    //  presenter.ButtonClicked();
                }
            }
        });

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");

        SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

        String newDateStr = postFormater.format(c);
        message_today.setText(newDateStr);



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        messsage.clear();
    }

    public BroadcastReceiver reciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };
    private boolean isOnLine(Context context){

        ConnectivityManager connectivityManager= (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if(info != null && info.isConnectedOrConnecting()){
            return true;
        }else {
            return false;
        }
    }
    private void Set_Visible_ON(){

    }

    private void Set_Visible_OFF(){

    }
    private void fromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Constantes.RC_PHOTO_PICKER);
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(reciver);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStop() {
        super.onStop();

    }


    @Override
    public void Wait() {

    }

    @Override
    public String getMessage() {
        return message_AppCompatEditText.getText().toString().trim();
    }

    @Override
    public void setMessage(Messsage message) {


        messsage.add(message);

//        Toast.makeText(getContext(),""+message,Toast.LENGTH_LONG).show();

        Log.d(TAG, "Información nueva: "+ message);
    }

    @Override
    public void onErrorgetMessage() {

    }

    @Override
    public void onClickMessage() {

    }
    public List<Messsage> Msg(){
        List<Messsage>msg_list = new ArrayList<>();
        Messsage messsage = new Messsage(0,"");
        msg_list.add( new Messsage(0,""));
        msg_list.add(new Messsage(0,""));
        msg_list.add(new Messsage(0,""));
        msg_list.add( new Messsage(0,""));

        return msg_list;
    }
}