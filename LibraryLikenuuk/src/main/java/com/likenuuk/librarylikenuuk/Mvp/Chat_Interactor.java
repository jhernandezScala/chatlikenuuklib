package com.likenuuk.librarylikenuuk.Mvp;

import com.likenuuk.librarylikenuuk.Model.Messsage;

public class Chat_Interactor implements Chat_MVP.Interactor{


    private Chat_Repository repository;
    private Chat_Presenter presenter;
    public Chat_Interactor(Chat_Presenter presenter) {
            this.repository = new Chat_Repository();
            this.presenter = presenter;

    }


    @Override
    public Messsage LoadChat(String message) {
        repository = new Chat_Repository();
        return repository.message();
    }

    @Override
    public Messsage SaveChat(String message) {
        repository = new Chat_Repository();
        repository.Savemessage(new Messsage(1,message));
        return repository.message();
    }
}
