package com.likenuuk.librarylikenuuk.Mvp;

import androidx.annotation.NonNull;

import com.likenuuk.librarylikenuuk.Model.Messsage;

public class Chat_Presenter implements Chat_MVP.Presenter {

    @NonNull
    private Chat_MVP.View view;
    private Chat_MVP.Interactor interactor;

    public Chat_Presenter(Chat_MVP.View view) {

        this.interactor = new Chat_Interactor(this);
    }

    @Override
    public void setView(Chat_MVP.View view) {

        this.view = view;
    }

    @Override
    public void ButtonClicked() {

        if(view!= null){

          view.setMessage(interactor.LoadChat(new Messsage(0, view.getMessage()).getMessage()));


        }

    }

    @Override
    public void getChatService() {

        if(view!= null){

            view.setMessage(interactor.LoadChat(new Messsage(0, view.getMessage()).getMessage()));


        }
    }
}
