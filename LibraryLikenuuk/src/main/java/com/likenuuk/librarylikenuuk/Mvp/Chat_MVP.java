package com.likenuuk.librarylikenuuk.Mvp;
import  com.likenuuk.librarylikenuuk.Model.Messsage;
public interface Chat_MVP {

    interface View{

       void Wait();
       String getMessage();

       void setMessage(Messsage message);
       void onErrorgetMessage();

    }
    interface  Presenter{

        void setView(Chat_MVP.View view);
        void ButtonClicked();

        void getChatService();

    }
    interface Interactor{

        Messsage LoadChat(String message);
        Messsage SaveChat(String message);

    }
    interface Chat_Repository{

        Messsage message();
        void Savemessage(Messsage message);

    }
    interface OnItemClickListenerMessage{

       void onClickMessage();

    }
}
