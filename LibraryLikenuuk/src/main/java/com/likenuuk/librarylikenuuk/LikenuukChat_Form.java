package com.likenuuk.librarylikenuuk;

import android.os.Parcel;
import android.os.Parcelable;

public class LikenuukChat_Form implements Parcelable {

    private String URL = "";


    private Boolean NAME = false;
    private Boolean EMAIL = false;
    private Boolean PHONE = false;

    public LikenuukChat_Form(String URL, Boolean NAME) {
        this.URL = URL;
        this.NAME = NAME;
    }

    public LikenuukChat_Form(String URL, Boolean NAME, Boolean EMAIL) {
        this.URL = URL;
        this.NAME = NAME;
        this.EMAIL = EMAIL;
    }

    public LikenuukChat_Form(String URL, Boolean NAME, Boolean EMAIL, Boolean PHONE) {
        this.URL = URL;
        this.NAME = NAME;
        this.EMAIL = EMAIL;
        this.PHONE = PHONE;
    }


    protected LikenuukChat_Form(Parcel in) {
        URL = in.readString();
        byte tmpNAME = in.readByte();
        NAME = tmpNAME == 0 ? null : tmpNAME == 1;
        byte tmpEMAIL = in.readByte();
        EMAIL = tmpEMAIL == 0 ? null : tmpEMAIL == 1;
        byte tmpPHONE = in.readByte();
        PHONE = tmpPHONE == 0 ? null : tmpPHONE == 1;
    }

    public static final Creator<LikenuukChat_Form> CREATOR = new Creator<LikenuukChat_Form>() {
        @Override
        public LikenuukChat_Form createFromParcel(Parcel in) {
            return new LikenuukChat_Form(in);
        }

        @Override
        public LikenuukChat_Form[] newArray(int size) {
            return new LikenuukChat_Form[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.URL);
        dest.writeBoolean(this.EMAIL);
        dest.writeBoolean(this.PHONE);
        dest.writeBoolean(this.NAME);
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Boolean getNAME() {
        return NAME;
    }

    public void setNAME(Boolean NAME) {
        this.NAME = NAME;
    }

    public Boolean getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(Boolean EMAIL) {
        this.EMAIL = EMAIL;
    }

    public Boolean getPHONE() {
        return PHONE;
    }

    public void setPHONE(Boolean PHONE) {
        this.PHONE = PHONE;
    }
}
