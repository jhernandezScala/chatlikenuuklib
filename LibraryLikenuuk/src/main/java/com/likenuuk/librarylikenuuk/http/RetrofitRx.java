package com.likenuuk.librarylikenuuk.http;

import android.content.Context;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRx {


    public OkHttpClient provideClientRx(final Context context) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        return new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                // HttpUrl url = request.url().newBuilder().addQueryParameter("Kuub ", getToken((context))).build();
                HttpUrl url = request.url().newBuilder().build();

                request = request.newBuilder().url(url).build();


                return chain.proceed(request);
            }
        }).build();
    }

    public Retrofit provideRetrofit(String baseUrl, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ApiClient providerRx(Context context, String Url) {

        return provideRetrofit(Url, provideClientRx(context)).create(ApiClient.class);
    }
}