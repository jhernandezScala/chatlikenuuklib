package com.likenuuk.librarylikenuuk.activityChat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.likenuuk.librarylikenuuk.ChatLikenuuk;
import com.likenuuk.librarylikenuuk.Contact_form;
import com.likenuuk.librarylikenuuk.LikenuukChat_Form;
import com.likenuuk.librarylikenuuk.R;

public class Activity_Likenuuk_chat extends AppCompatActivity {
    ChatLikenuuk chatLikenuuk;

    FragmentManager manager;
    FragmentTransaction transaction;
    private Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_likenuuk_chat);

      Intent intent = new Intent();


     //   bundle= intent.getExtras();

       // Toast.makeText(getApplicationContext(),""+bundle.getString("id") ,Toast.LENGTH_LONG).show();


        if (savedInstanceState == null)

        {
            chatLikenuuk = new ChatLikenuuk();

            LikenuukChat_Form likenuukChat_form = new LikenuukChat_Form("",true,true,true);
            //chatLikenuuk = ChatLikenuuk.newInstance(init);

            Contact_form contact_form = Contact_form.newInstance(likenuukChat_form);

            manager = getSupportFragmentManager();
            transaction = manager.beginTransaction();
            //transaction.replace(R.id.fragment, chatLikenuuk);
            transaction.replace(R.id.fragment, contact_form);

            transaction.disallowAddToBackStack();

            transaction.commit();
        }
    }
}