package com.likenuuk.librarylikenuuk.Service;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.likenuuk.librarylikenuuk.ChatW.ChatWeb;
import com.likenuuk.librarylikenuuk.R;
import com.likenuuk.librarylikenuuk.activityChat.Activity_Likenuuk_chat;

public class Bubble_Chat extends Service {
    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;
    private int lastAction;



    Intent inten_;
    private String usr ;

    private View view;
    private WindowManager wm;

    private WindowManager wm_;
    private View view_;
    public Bubble_Chat() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
     /*   String value = intent.getExtras().getString("id");
        usr = value;

        if (intent != null && intent.getExtras() != null) {
            inten_ = intent;



            Log.d("MY_TAG_1", "Received intent with action=" + value + "; now what?");

        }else{
            usr = "notFound_1";
        }*/
        ConstraintLayout ll;
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        view = LayoutInflater.from(this).inflate(R.layout.bubble_chat, null);

        ImageView close = view.findViewById(R.id.closed);

      /*  ll.setBackgroundColor(Color.RED);
        LinearLayout.LayoutParams layoutParameteres = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 400);
        ll.setBackgroundColor(Color.argb(66,255,0,0));
        ll.setLayoutParams(layoutParameteres);*/



        final WindowManager.LayoutParams parameters ;




        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M ) {
            parameters = new WindowManager.LayoutParams(
                    180, 180, WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

        }else{

            parameters = new WindowManager.LayoutParams(
                    180, 180, WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

        }
        parameters.gravity = Gravity.CENTER | Gravity.CENTER;
        parameters.x = 0;
        parameters.y = 0;


        Button     stop = new Button(this);
        stop.setText("Stop");
        ViewGroup.LayoutParams btnParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        stop.setLayoutParams(btnParameters);

        // ll.addView(stop);
        wm.addView(view, parameters);

        view.setOnTouchListener(new View.OnTouchListener() {
            WindowManager.LayoutParams updatedParameters = parameters;
            double x;
            double y;
            double pressedX;
            double pressedY;
            //variable for storing the time of first click
            long startTime;
            //constant for defining the time duration between the click that can be considered as double-tap
            static final int MAX_DURATION = 200;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    startTime = System.currentTimeMillis();
                }
                else if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if(System.currentTimeMillis() - startTime <= MAX_DURATION)
                    {

                        //DOUBLE TAP

                        // wm_.addView(view_,parameters_);
                        Intent intent = new Intent(Bubble_Chat.this, ChatWeb.class);
                        //Bundle mBundle = new Bundle();
                       // mBundle.putString("id", intent.getExtras().getString("id"));
                       // intent.putExtras(mBundle);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                }
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        x = updatedParameters.x;
                        y = updatedParameters.y;

                        pressedX = event.getRawX();
                        pressedY = event.getRawY();

                        break;

                    case MotionEvent.ACTION_MOVE:
                        updatedParameters.x = (int) (x + (event.getRawX() - pressedX));
                        updatedParameters.y = (int) (y + (event.getRawY() - pressedY));

                        wm.updateViewLayout(view, updatedParameters);

                    default:
                        break;
                }

                return false;
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wm.removeView(view);
                stopSelf();
                System.exit(0);
            }
        });
        return START_REDELIVER_INTENT;
    }




    private void SaveData(String value) {

        usr = value;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        if (intent != null && intent.getExtras() != null) {
            inten_ = intent;
            String value = intent.getExtras().getString("id");
            usr = value;
            SaveData(value);
            Log.d("MY_TAG_2", "Received intent with action=" + value + "; now what?");

        }else{
            usr = "notFound_2";
        }

        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public void onCreate() {
        super.onCreate();


    }

}