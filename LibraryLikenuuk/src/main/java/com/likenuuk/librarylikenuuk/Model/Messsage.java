package com.likenuuk.librarylikenuuk.Model;

public class Messsage {

    private int typeUser;
    private String message;


    public Messsage(int typeUser, String message) {
        this.typeUser = typeUser;
        this.message = message;
    }

    public int getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(int typeUser) {
        this.typeUser = typeUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
