package com.likenuuk.librarylikenuuk;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.likenuuk.librarylikenuuk.Model.Messsage;
import com.likenuuk.librarylikenuuk.Mvp.Chat_MVP;
import com.likenuuk.librarylikenuuk.Mvp.Chat_Presenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Contact_form#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Contact_form extends Fragment implements Chat_MVP.View, Chat_MVP.OnItemClickListenerMessage{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static LikenuukChat_Form likenuukChatInit;

    private MaterialTextView name;
    private MaterialTextView phone;
    private MaterialTextView email;
    private Button button;
    private TextInputLayout container_name;
    private TextInputLayout container_phone;
    private TextInputLayout getContainer_email;
    private TextInputEditText input_name;
    private TextInputEditText input_email;
    private TextInputEditText input_phone;
    private ScrollView scroll_container;
    private ConstraintLayout container_chat;

    // TODO: layout chat
    private final String TAG =getTag();
    private ConstraintLayout layout_chat;
    private AdapterChat adapterChat;
    private ImageButton btnSendMessage;
    private ImageButton btnGallery;
    private AppCompatEditText message_AppCompatEditText;
    private List<Messsage> messsage= new ArrayList<>();
    private RecyclerView recyclerView;
    private MaterialTextView message_today;

    private Chat_Presenter presenter;


    private LinearLayout progressBar;



    public Contact_form() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Contact_form newInstance(LikenuukChat_Form likenuukChat_form) {
        Contact_form fragment = new Contact_form();
        Bundle args = new Bundle();
        args.putParcelable("", likenuukChat_form);
        likenuukChatInit = likenuukChat_form;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
          //  mParam1 = getArguments().getString(ARG_PARAM1);
          //  mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

   private View view;
    private Boolean disponible_email = false;
    private Boolean disponible_name = false;
    private Boolean disponible_phone = false;
    private Animation in, out;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact_form, container, false);



      initView(view);
        return view;
    }

    private void initView(View view) {

        name = view.findViewById(R.id.text_name);
        phone = view.findViewById(R.id.text_phone);
        email = view.findViewById(R.id.text_email);
        button = view.findViewById(R.id.button);
        container_name = view.findViewById(R.id.container_input_name);
        container_phone = view.findViewById(R.id.container_input_phone);
        getContainer_email = view.findViewById(R.id.container_input_email);
        input_email = view.findViewById(R.id.input_email);
        input_phone = view.findViewById(R.id.input_phone);
        input_name = view.findViewById(R.id.input_name);
        progressBar = view.findViewById(R.id.container_progress);
        in = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
        out = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out);
        scroll_container = view.findViewById(R.id.scroll_container);
        disponible_email =  likenuukChatInit.getEMAIL();
        disponible_name = likenuukChatInit.getNAME();
        disponible_phone = likenuukChatInit.getPHONE();
        container_chat = view.findViewById(R.id.layout_chat);
        progressBar.setVisibility(View.GONE);
        container_chat.setVisibility(View.GONE);
        presenter = new Chat_Presenter(this);
        presenter.setView(this);

        if (disponible_phone){
            container_phone.setVisibility(View.VISIBLE);
            phone.setVisibility(View.VISIBLE);
        }else{
            container_phone.setVisibility(View.GONE);
            phone.setVisibility(View.GONE);
        }
        if(disponible_name){
            container_name.setVisibility(View.VISIBLE);
            name.setVisibility(View.VISIBLE);
        }else{
            container_phone.setVisibility(View.GONE);
            name.setVisibility(View.GONE);
        }
        if(disponible_email){
            email.setVisibility(View.VISIBLE);
            getContainer_email.setVisibility(View.VISIBLE);
        }else {
            getContainer_email.setVisibility(View.GONE);
            email.setVisibility(View.GONE);

        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!input_name.getText().toString().isEmpty()){
                    progressBar.startAnimation(in);
                    scroll_container.startAnimation(out);
                    scroll_container.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);

                    container_chat.startAnimation(in);
                    container_chat.setVisibility(View.VISIBLE);
                    progressBar.startAnimation(out);
                    progressBar.setVisibility(View.GONE);
                }else{
                    Snackbar.make(button,"Requiere name", BaseTransientBottomBar.LENGTH_LONG).show();
                }



            }
        });

        /*
        *
        * */

        layout_chat = view.findViewById(R.id.layout_chat);
        btnGallery = view.findViewById(R.id.btnGallery);
        btnSendMessage = view.findViewById(R.id.btnSendMessage);
        recyclerView = view.findViewById(R.id.recyclerView);
        message_today = view.findViewById(R.id.message_today);

        message_AppCompatEditText = view.findViewById(R.id.etMessage);

        adapterChat = new AdapterChat(messsage,getContext(),this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapterChat);


        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                messsage.add(new Messsage(0,"Como puedo ayudarlo?"));
                adapterChat.notifyItemInserted(messsage.size()-1);
                recyclerView.getLayoutManager().scrollToPosition(messsage.size()-1);

                //   Toast.makeText(getContext(),"click",Toast.LENGTH_LONG).show();
            }
        });

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!message_AppCompatEditText.getText().toString().isEmpty()) {
                    messsage.add(new Messsage(1,message_AppCompatEditText.getText().toString()));
                    adapterChat.notifyItemInserted(messsage.size()-1);
                    recyclerView.getLayoutManager().scrollToPosition(messsage.size()-1);

                    message_AppCompatEditText.setText("");

                    //  presenter.ButtonClicked();
                }
            }
        });

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");

        SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

        String newDateStr = postFormater.format(c);
        message_today.setText(newDateStr);

    }

    @Override
    public void onClickMessage() {

    }

    @Override
    public void Wait() {

    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public void setMessage(Messsage message) {

    }

    @Override
    public void onErrorgetMessage() {

    }
}
