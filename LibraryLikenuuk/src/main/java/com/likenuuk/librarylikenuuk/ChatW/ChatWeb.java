package com.likenuuk.librarylikenuuk.ChatW;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.likenuuk.librarylikenuuk.R;

public class ChatWeb extends AppCompatActivity {
    private WebView mwebview;
    private SwipeRefreshLayout swipeRefreshLayout;

    private Bundle bundle= new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_web);

        mwebview = findViewById(R.id.webView);

        if (savedInstanceState != null)
        {
            bundle = savedInstanceState;
          mwebview.saveState(savedInstanceState);

         }else {
            String URL = "https://chatfmp.likenuuk.com/nmp";
            mwebview.loadUrl(URL);

        }
        mwebview.requestFocus();
        mwebview.getSettings().setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath()+"/cache");
        mwebview.getSettings().setDatabasePath(getApplicationContext().getFilesDir().getAbsolutePath()+"/databse");

        WebSettings webSettings= mwebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mwebview.setWebChromeClient(new WebChromeClient());
        webSettings.setDomStorageEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);


        //mwebview.loadUrl("https://stackoverflow.com/questions/10241633/android-progressbar-countdown");
        mwebview.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                Log.i("TAG", "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }
            public void onPageFinished(WebView view, String url){
                Log.i("TAG", "Finished loading url: " +url);
                //if (progressBar.isShowing()){
                //  progressBar.dismiss();
                // }
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
                Log.e("TAG", "Error: " + description);
                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                // alertDialog.setTitle("Error");
                //alertDialog.setMessage(description);
                //alertDialog.show();
            }
        });
        WebAppInterface webAppInterface= new WebAppInterface(this);




    }



    @Override
    protected void onStart() {
        super.onStart();
        onRestoreInstanceState(bundle);
    }

    @Override
    protected void onStop() {
        onSaveInstanceState(bundle);
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        mwebview.saveState(outState);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        mwebview.restoreState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);

    }

    public class WebAppInterface {
        Context mContext;


        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }
    }


}