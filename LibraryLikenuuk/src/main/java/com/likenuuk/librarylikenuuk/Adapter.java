package com.likenuuk.librarylikenuuk;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.likenuuk.librarylikenuuk.Model.Messsage;
import com.likenuuk.librarylikenuuk.Mvp.Chat_MVP;

import java.util.List;

class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder>{

    private List<Messsage> list_message;
    private Context context;
    private Chat_MVP.OnItemClickListenerMessage messageOnclick;
    public AdapterChat(List<Messsage> list_message, Context context,Chat_MVP.OnItemClickListenerMessage messageOnclick) {

        this.list_message = list_message;
        this.context = context;
        this.messageOnclick = messageOnclick;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_chat, parent,false);
        return new ViewHolder(view, messageOnclick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (list_message.get(position).getTypeUser() == 0){

            holder.message_serve.setText("Serve: "+list_message.get(position).getMessage());

            holder.message_serve.setVisibility(View.VISIBLE);
            holder.message.setVisibility(View.INVISIBLE);
        }
        if (list_message.get(position).getTypeUser() == 1){

            holder.message_serve.setVisibility(View.INVISIBLE);
            holder.message.setVisibility(View.VISIBLE);
            holder.message.setText("user: "+list_message.get(position).getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return list_message.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public MaterialTextView message;
        public MaterialTextView message_serve;


       Chat_MVP.OnItemClickListenerMessage onItemClickListener;

        public ViewHolder(@NonNull View itemView ,Chat_MVP.OnItemClickListenerMessage itemClickListener) {
            super(itemView);

            message = itemView.findViewById(R.id.tvMessage);
            message_serve = itemView.findViewById(R.id.tvMessage_serve);

            onItemClickListener = itemClickListener;
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {

          /*  onItemClickListener.onClick(list.getOrders().get(getAdapterPosition()).getId(),
                    list.getOrders().get(getAdapterPosition()).getBranch().getUrl(),
                    list.getOrders().get(getAdapterPosition()).getBranch().getStreet()+
                            list.getOrders().get(getAdapterPosition()).getBranch().getStreetNumber()+
                            list.getOrders().get(getAdapterPosition()).getBranch().getCity(),
                    // list.get(getAdapterPosition()).getId_status()
                    getAdapterPosition(),

                    String.valueOf(list.getOrders().get(getAdapterPosition()).getBranch().getLongitude()),
                    String.valueOf(list.getOrders().get(getAdapterPosition()).getBranch().getLatitude())
            );*/
            try {
                //onItemClickListener.onClickmyFavor(
                  //                                        );
            }catch (Exception e){
                Log.e("TAG" +e.getCause(), e.getMessage());
            }

        }





}
}
